package main

import (
	"log"
	"net/http"
	"gitlab.com/thalesfaggiano/ge-rate-limiter/internal/application"
	"gitlab.com/thalesfaggiano/ge-rate-limiter/internal/infra/config"
	"gitlab.com/thalesfaggiano/ge-rate-limiter/internal/infra/limiter"
	"gitlab.com/thalesfaggiano/ge-rate-limiter/internal/domain/service"
	"gitlab.com/thalesfaggiano/ge-rate-limiter/internal/infra/middleware"
)

func main() {
	cfg, err := config.LoadConfig(".env")
	if err != nil {
		log.Fatalf("Erro configuracao: %v", err)
	}
	log.Printf("Configuracao Redis: host=%s, port=%s, password=%s", cfg.RedisHost, cfg.RedisPort, cfg.RedisPassword)

	redisAddr := cfg.RedisHost + ":" + cfg.RedisPort
	ipStore := limiter.NewRedisStore(redisAddr, cfg.RedisPassword)
	tokenStore := limiter.NewRedisStore(redisAddr, cfg.RedisPassword)

	rateLimiter := limiter.NewLimiter(
		cfg.IPMaxRequestsPerSecond,
		cfg.IPBlockDurationSeconds,
		cfg.TokenMaxRequestsPerSecond,
		cfg.TokenBlockDurationSeconds,
		ipStore,
		tokenStore,
	)

	rateLimiterService := service.NewRateLimiterService(rateLimiter)
	rateLimiterApp := application.NewRateLimiterApp(rateLimiterService)
	rateLimiterMiddleware := middleware.NewRateLimiterMiddleware(rateLimiterApp)

	mux := http.NewServeMux()
	mux.Handle("/", rateLimiterMiddleware.Handler(http.HandlerFunc(handler)))

	log.Println("Server executado na porta 8080")
	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Fatalf("Erro ao iniciar o servidor: %v", err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Olá, mundo!\n"))
}
