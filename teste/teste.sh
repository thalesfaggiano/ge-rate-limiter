#!/bin/bash
re='^[0-9]+$'
[[ $1 =~ $re ]] && {
	echo Iniciando script... 
} || { 
	echo "Parâmetros incorretos." 
	exit 1
}

for x in $(seq $1);
do
	echo "Repetição $x ..."
	[[ -z $2 ]] && {
		echo 'curl http://localhost:8080' 
		curl http://localhost:8080 
	} || {
		echo 'curl -H "TOKEN:' $2'" http://localhost:8080'
		curl -H "TOKEN: $2" http://localhost:8080
	}
	echo "---------------"
done
