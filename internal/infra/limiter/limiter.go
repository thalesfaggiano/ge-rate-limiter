package limiter

import (
	"fmt"
	"log"
	"time"
	"strconv"
)

type Limiter struct {
	IPMaxRequestsPerSecond    int
	IPBlockDurationSeconds    int
	TokenMaxRequestsPerSecond int
	TokenBlockDurationSeconds int
	IPStore                   Store
	TokenStore                Store
}

func NewLimiter(ipMaxRequestsPerSecond, ipBlockDurationSeconds, tokenMaxRequestsPerSecond, tokenBlockDurationSeconds int, ipStore, tokenStore Store) *Limiter {
	return &Limiter{
		IPMaxRequestsPerSecond:    ipMaxRequestsPerSecond,
		IPBlockDurationSeconds:    ipBlockDurationSeconds,
		TokenMaxRequestsPerSecond: tokenMaxRequestsPerSecond,
		TokenBlockDurationSeconds: tokenBlockDurationSeconds,
		IPStore:                   ipStore,
		TokenStore:                tokenStore,
	}
}

func (l *Limiter) AllowRequest(ip, token string) bool {
	if token != "" {
		return l.isAllowedByToken(token)
	}
	return l.isAllowedByIP(ip)
}

func (l *Limiter) isAllowedByIP(ip string) bool {
	key := fmt.Sprintf("ratelimit:ip:%s", ip)
	log.Printf("Checando Rate Limit para o IP: %s", ip)
	return l.isAllowed(key, l.IPMaxRequestsPerSecond, l.IPBlockDurationSeconds, l.IPStore)
}

func (l *Limiter) isAllowedByToken(token string) bool {
	key := fmt.Sprintf("ratelimit:token:%s", token)
	log.Printf("Checando Rate Limite para o TOKEN: %s", token)
	return l.isAllowed(key, l.TokenMaxRequestsPerSecond, l.TokenBlockDurationSeconds, l.TokenStore)
}

func (l *Limiter) isAllowed(key string, maxRequestsPerSecond, blockDurationSeconds int, store Store) bool {
	countStr, err := store.Get(key)
	if err != nil {
		log.Printf("Erro ao pegar informacoes da store: %v", err)
		return false
	}

	count, _ := strconv.Atoi(countStr)
	log.Printf("TOKEN: %s, Contagem atual: %d, Requisicoes Maximas: %d", key, count, maxRequestsPerSecond)
	if count >= maxRequestsPerSecond {
		log.Printf("Rate limit excedido para o TOKEN: %s", key)
		return false
	}

	expiration := time.Duration(blockDurationSeconds) * time.Second
	if err := store.Incr(key); err != nil {
		log.Printf("Erro incrementando o TOKEN: %s, error: %v", key, err)
		return false
	}
	if err := store.Expire(key, expiration); err != nil {
		log.Printf("TOKEN expirado: %s, error: %v", key, err)
		return false
	}

	log.Printf("Requisições autorizadas para o TOKEN: %s", key)
	return true
}
