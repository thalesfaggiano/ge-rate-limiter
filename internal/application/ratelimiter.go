package application

import (
	"gitlab.com/thalesfaggiano/ge-rate-limiter/internal/domain/service"
)

type RateLimiterServiceInterface interface {
	AllowRequest(ip, token string) bool
}

type RateLimiterApp struct {
	RateLimiterService *service.RateLimiterService
}

func NewRateLimiterApp(rateLimiterService *service.RateLimiterService) *RateLimiterApp {
	return &RateLimiterApp{
		RateLimiterService: rateLimiterService,
	}
}

func (app *RateLimiterApp) AllowRequest(ip, token string) bool {
	return app.RateLimiterService.AllowRequest(ip, token)
}
