FROM golang:1.22.3-alpine

WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod tidy
RUN go mod download
COPY . .
RUN go build -o servidor ./cmd/server

CMD ["./servidor"]
